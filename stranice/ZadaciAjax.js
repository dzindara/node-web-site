var ZadaciAjax = (function(){
    var konstruktor = function(callbackFn){
        var radi=false;
        var ajax;
    return {
    dajXML:function(){
        if(radi){
            callbackFn(JSON.stringify({greska:"Već ste uputili zahtjev"}));
        }else{

            ajax = new XMLHttpRequest();
            ajax.onreadystatechange = function() {// Anonimna funkcija
                if (ajax.readyState == 4 && ajax.status == 200){
                    radi=false;
                    callbackFn(ajax.response);
                }
            }

            radi=true;
            ajax.open("GET","http://localhost:8080/zadaci",true);
            //_____
            ajax.timeout=2000;
            ajax.ontimeout=function(e){
               radi=false;
               ajax.abort();
            };
            //_____
            ajax.setRequestHeader('Accept','application/xml');
            ajax.send();
        }   

    },
    dajCSV:function(){
        ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {// Anonimna funkcija
            if (ajax.readyState == 4 && ajax.status == 200){
                radi=false;
                callbackFn(ajax.response);
            }

        }

        radi=true;
        ajax.open("GET","http://localhost:8080/zadaci",true);
        //_____
        ajax.timeout=2000;
        ajax.ontimeout=function(e){
            radi=false;
            ajax.abort();
        };
        //_____
        ajax.setRequestHeader('Accept','text/csv');
        ajax.send();
    },
    dajJSON:function(){
        ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {// Anonimna funkcija
            if (ajax.readyState == 4 && ajax.status == 200){
                radi=false;
                callbackFn(ajax.response);
            }

        }
        radi=true;
        ajax.open("GET","http://localhost:8080/zadaci",true);
        //_____
        ajax.timeout=2000;
        ajax.ontimeout=function(e){
            radi=false;
            ajax.abort();
        };
        //_____
        ajax.setRequestHeader('Accept','application/json');
        ajax.send();
    }
    }
}
    return konstruktor;
}());

module.exports=ZadaciAjax;