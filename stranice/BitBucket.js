var BitBucket = (function () {
    var konstruktor = function (key, secret) {
        var mojPromise = new Promise(function (resolve, reject) {
            
        
                var ajax = new XMLHttpRequest();

                ajax.onreadystatechange = function () {
                    if (ajax.readyState == 4 && ajax.status == 200)
                        resolve(JSON.parse(ajax.responseText).access_token);
                    else if (ajax.readyState == 4)
                        reject(ajax.status);
                }
                ajax.open("POST", "https://bitbucket.org/site/oauth2/access_token", true);
                ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                ajax.setRequestHeader("Authorization", 'Basic ' + btoa(key + ":" + secret));
                ajax.send("grant_type=" + encodeURIComponent("client_credentials"));
            

           
        });
        return {
            ucitaj: function (nazivRepSpi, nazivRepVje, cb) {
                mojPromise.then(function (token) {
                    let link = "https://api.bitbucket.org/2.0/repositories?role=member&q=(name~\"" + nazivRepSpi + "\" OR name~\"" + nazivRepVje + "\")";
                    var ajax1 = new XMLHttpRequest();
                    ajax1.onreadystatechange = function () {
                        if (ajax1.readyState == 4 && ajax1.status == 200) {
                            var primi = JSON.parse(ajax1.responseText);
                            var jsonRez = primi.values;
                            var niz = [];
                            for (var i = 0; i < jsonRez.length; i++) {

                                var objekat = {
                                    imePrezime: jsonRez[i].owner.username,
                                    index: jsonRez[i].full_name.substr(jsonRez[i].full_name.length - 5, 5)
                                };




                                if (!niz.find((student) => student.index == objekat.index)) {

                                    niz.push(objekat);
                                }
                            }
                            cb(null, niz);
                        }
                        
                        else if(ajax1.readyState==4) {
                            cb(ajax1.status, null);}
                    }
                    ajax1.open("GET", link);
                    ajax1.setRequestHeader("Authorization", 'Bearer ' + token);
                    ajax1.send();
                }).catch(function (error) {
                    cb(error, null);
                });
            }
        }





    }

    return konstruktor;


}());