var Validacija=(function () {  
    var errori=[];

 

    var konstruktor=function(divElementPoruke){
        function upisiUErrore(stringValidacije,inputElement){
            inputElement.style.backgroundColor="orangered";
            errori.push(stringValidacije);
        }
        function obrisiIzErrora(stringValidacije,inputElement){
            console.log("naziv:"+errori+"!"+(errori.indexOf(stringValidacije)));
            inputElement.style.backgroundColor="white";
            errori.splice(errori.indexOf(stringValidacije),1);
            console.log("naziv:"+errori);
        }
        function ispis(){
            console.log(errori);
            if(errori.length==0){
                divElementPoruke.innerHTML="";
            }
            else{
                divElementPoruke.innerHTML="Sljedeća polja nisu validna:"+errori.join(',')+"!";
                divElementPoruke.style.backgroundColor="orangered";
            }
        }
        return{
    
            ime:function(inputElement){
                //uradjeno
                var regex = /^([A-Z]'?[A-Za-z]('?[A-Za-z])*'?)([ -][A-Z]'?[A-Za-z]('?[A-Za-z])*'?){0,3}\s?$/;
                var i=0;
                if(regex.test(inputElement.value)){
                    
                    i=1;
                    if(inputElement.style.backgroundColor=="orangered"){
                        obrisiIzErrora("ime",inputElement);
                    }
                    
               }
               else{
                  
                   i=-1;
                   if(inputElement.style.backgroundColor=="" ||inputElement.style.backgroundColor=="white" ){
                       
                       upisiUErrore("ime",inputElement);
                   }
               }
               ispis();



               return i;


                
            },
            godina:function(inputElement){
                //URAĐENO
                var i=0;
                console.log(inputElement.value.length);
                if(inputElement.value.length!=9){
                    i= -1;
                }
                var regex = /^20\d\d\/20\d\d$/;
                if(!regex.test(inputElement.value)){
                    i= -1;
                }
            
                var ab=parseInt(inputElement.value[2]+inputElement.value[3]);
                var cd=parseInt(inputElement.value[7]+inputElement.value[8]);
                if(ab+1!=cd){
                    i= -1;
                }else if(i==0){
                    i=1;
                    
                }else{}

                if(i==1){
                    
                    
                    if(inputElement.style.backgroundColor=="orangered"){
                        obrisiIzErrora("godina",inputElement);
                    }
                    
               }
               else{
                  
                   
                   if(inputElement.style.backgroundColor=="" ||inputElement.style.backgroundColor=="white" ){
                       
                       upisiUErrore("godina",inputElement);
                   }
               }
               ispis();



               return i;

            },
            repozitorij:function(inputElement,regex){
                //URAĐEN
               var i;
                if(regex.test(inputElement.value)){
                    
                    i=1;
                    if(inputElement.style.backgroundColor=="orangered"){
                        obrisiIzErrora("repozitorij",inputElement);
                    }
               }
               else{
                  
                   i=-1;
                   if(inputElement.style.backgroundColor=="" ||inputElement.style.backgroundColor=="white" ){
                       
                       upisiUErrore("repozitorij",inputElement);
                   }
               }
               ispis();

            return i;

            },
            index:function(inputElement){
                //URAĐEN
                var i;
                if(inputElement.value.length!=5){
                    i= -1;
                    console.log("krec");
                }
                else{
                    if(parseInt(inputElement.value[0]+inputElement.value[1])<14 ||parseInt(inputElement.value[0]+inputElement.value[1]>20)){
                        i= -1;
                        console.log("skrec");
                    }
                    else {
                        i=1;
                        console.log("mec");
                    }
                }

                if(i==1){
                    if(inputElement.style.backgroundColor=="orangered"){
                        obrisiIzErrora("index",inputElement);
                    }
               }
               else{
                   if(inputElement.style.backgroundColor=="" ||inputElement.style.backgroundColor=="white" ){
                       upisiUErrore("index",inputElement);
                   }
               }
               ispis();



               return i;




            },
            naziv:function(inputElement){
                //URAĐEN
                var regex=/^[A-Za-z][,;:?!'"\-\/\\0-9A-Za-z]+[a-z0-9]$/;
                var i;
               
                if(regex.test(inputElement.value)){
                    
                     i=1;
                     if(inputElement.style.backgroundColor=="orangered"){
                         obrisiIzErrora("naziv",inputElement);
                     }
                     
                }
                else{
                   
                    i=-1;
                    if(inputElement.style.backgroundColor=="" ||inputElement.style.backgroundColor=="white" ){
                        
                        upisiUErrore("naziv",inputElement);
                    }
                }
                ispis();



                return i;
            },
            password:function(inputElement){
                //URAĐEN
                var i;
                if(inputElement.value.length<8){
                    i= -1;
                }
                else{
                    var malo=0,veliko=0,broj=0;
                    var expmalo=/[a-z]/;
                    var expveliko=/[A-Z]/;
                    var expbroj=/[0-9]/;
                    for(var i=0;i<inputElement.value.length;i++){
                        if(expmalo.test(inputElement.value[i])){
                            malo++;
                        }
                        else if(expveliko.test(inputElement.value[i])) {
                            veliko++;
                        }
                        else if(expbroj.test(inputElement.value[i])){
                            broj++;
                        }
                        else {
                            i= -1;
                        }
                    }
            
                    if((malo==0 &&(veliko==0 || broj==0)) || (broj==0 && veliko==0)){
                        i= -1;
                    }
                    else i=1;
                }

                if(i==1){
                    
                    
                    if(inputElement.style.backgroundColor=="orangered"){
                        obrisiIzErrora("password",inputElement);
                    }
                    
               }
               else{
                  
                   
                   if(inputElement.style.backgroundColor=="" ||inputElement.style.backgroundColor=="white" ){
                       
                       upisiUErrore("password",inputElement);
                   }
               }
               ispis();



               return i;

                
            },
            url:function(inputElement){
                //urađen
                var regex=/^(http|https|ftp|ssh):\/\/([a-z0-9]+([a-z0-9\-]*[a-z0-9])*\.)*[a-z0-9]+([a-z0-9\-]*[a-z0-9])*(\/[a-z0-9]+([a-z0-9\-]*[a-z0-9])*)*(\?([a-z0-9]+([a-z0-9\-]*[a-z0-9])*\=[a-z0-9]+([a-z0-9\-]*[a-z0-9])*)(\&[a-z0-9]+([a-z0-9\-]*[a-z0-9])*\=[a-z0-9]+([a-z0-9\-]*[a-z0-9])*)*)?$/;
                var i;
                if(regex.test(inputElement.value)){
                    
                    i=1;
                    if(inputElement.style.backgroundColor=="orangered"){
                        obrisiIzErrora("url",inputElement);
                    }
                    
               }
               else{
                  
                   i=-1;
                   if(inputElement.style.backgroundColor=="" ||inputElement.style.backgroundColor=="white" ){
                       
                       upisiUErrore("url",inputElement);
                   }
               }
               ispis();



               return i;
            }
        }
    }
    return konstruktor;
}());