//Osnovne deklaracije i postavke
const express = require('express');
const fs = require('fs');
const url = require('url');
const bodyParser = require('body-parser');
const multer = require('multer');
const path = require('path');
const dir = require('node-dir');
var json2xml = require('json2xml');

var urlencodedParser = bodyParser.urlencoded({ extended: false });
//____s4_____
const Sequelize = require('sequelize');
const db = require('./db.js');
db.sequelize.sync().then(function () {
    console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
}).catch(function(err){
    console.log("Konekcija na bazu nije uspjela!");
});
//____s4_____

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './postavke');
    },
    filename: function (req, file, cb) {
        cb(null, req.body.naziv + '.pdf');
    }
});

var upload = multer({
    storage: storage,
    fileFilter: function (req, file, cb) {

        if (req.body.naziv === '') {

            req.fileValidationError = 'Upisite naziv fajla!';
            return cb(null, false, new Error('Upisite naziv fajla!'));
        }

        if (path.extname(file.originalname) !== '.pdf') {

            req.fileValidationError = 'Ekstenzija nije pdf';
            return cb(null, false, new Error('Ekstenzija nije pdf!'));
        }



        cb(null, true);
    }
}).single('postavka');

const app = express();
//Prvi
app.use(express.static('stranice'));
app.use(express.static('postavke'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//Pug
app.set('views', __dirname);
app.set('view engine', 'pug');

//Drugi
app.post('/addZadatak', function (req, res) {
    if (!fs.existsSync('./postavke')) {

        fs.mkdirSync('./postavke');
    }

    upload(req, res, function (err) {


        if (req.fileValidationError) {
            res.render('greska', { url: '/addZadatak.html', poruka: req.fileValidationError });
        }
        else if (err) {
            res.render('greska', { url: '/addZadatak.html', poruka: err });
        }
        else if (req.file == null) {
            res.render('greska', { url: '/addZadatak.html', poruka: 'Odaberite fajl!' });
        }
        else {
            if (req.body.naziv) {
                db.zadatak.find({ where: { naziv: req.body.naziv } }).then(function (zadatakIzBaze) {
                    if (zadatakIzBaze == null) {
                        db.zadatak.create({ naziv: req.body.naziv, postavka: 'http://localhost:8080/postavke/' + req.body.naziv + '.pdf' });
                        var objekat = { naziv: req.body.naziv, postavka: 'http://localhost:8080/postavke/' + req.body.naziv + '.pdf' };
                        var mojJSON = JSON.stringify(objekat);

                        res.writeHead(200, { 'Content-Type': "application/json" });
                        res.end(mojJSON);
                    }
                    else {
                        res.render('greska', { url: '/addZadatak.html', poruka: 'Pdf sa ovim nazivom vec postoji.' });
                    }
                }).catch(function (err) {
                    
                    res.render('nemaZadatka', { naziv: 'Error', poruka: err });
                });
            }
        }

    });



});

//Treci
app.get('/zadatak', function (req, res) {
    if (req.query.naziv) {
        var putanja = req.query.naziv;
        db.zadatak.find({ where: { naziv: putanja } }).then(function (nazivZadatka) {
            if (nazivZadatka == null) {
                res.render('nemaZadatka', { naziv: 'Zadatak nije pronadjen', poruka: 'Nema zadatka sa tim nazivom!' });
            }
            else {
                res.sendFile(__dirname + '/postavke/' + nazivZadatka.naziv + '.pdf');
            }
        }).catch(function (err) {
            
            res.render('nemaZadatka', { naziv: 'Error', poruka: err });
        });
       
    }



});

//Cetvrti
app.post('/addGodina', function (req, res) {
    if (req.body.nazivGod && req.body.nazivRepVje && req.body.nazivRepSpi) {
        if (req.body.nazivGod != '' && req.body.nazivRepVje != '' && req.body.nazivRepSpi != '') {
            var godina = req.body.nazivGod;
            var repVjezba = req.body.nazivRepVje;
            var repSpirala = req.body.nazivRepSpi;
            db.godina.find({ where: { nazivGod: godina } }).then(function (godinaIzBaze) {


                if (godinaIzBaze == null) {
                    db.godina.create({ nazivGod: godina, nazivRepVje: repVjezba, nazivRepSpi: repSpirala });

                    res.sendFile(__dirname + '/stranice/addGodina.html');
                }
                else {
                    res.render('greska', { url: '/addGodina.html', poruka: 'Godina već postoji u tabeli godine.' });
                }
            }).catch(function (err) {
              
                res.render('nemaZadatka', { naziv: 'Error', poruka: err });
            });
        }
    }
});

//Peti
app.get('/godine', function (req, res) {

    var niz = [];
    db.godina.findAll({
        attributes: ['nazivGod', 'nazivRepVje', 'nazivRepSpi']
    }).then(function (redovi) {
        for (var i = 0; i < redovi.length; i++) {

            var red = { nazivGod: redovi[i].dataValues.nazivGod, nazivRepVje: redovi[i].dataValues.nazivRepVje, nazivRepSpi: redovi[i].dataValues.nazivRepSpi };

            niz.push(red);
        }


        res.writeHead(200, { 'Content-Type': "application/json" });
        res.end(JSON.stringify(niz));

    }).catch(function (err) {
        
        res.render('nemaZadatka', { naziv: 'Error', poruka: err });
    });

});


//Sedmi
app.get('/zadaci', function (req, res) {

    //provjera header-a
    var header = req.headers.accept;

    var trebaJSON = header.indexOf('application/json');

    var trebaXML = header.indexOf('application/xml');

    var trebaXML2 = header.indexOf('text/xml');

    var trebaCSV = header.indexOf('text/csv');

    var niz = [];


    db.zadatak.findAll({
        attributes: ['naziv', 'postavka']
    }).then(function (redovi) {
        for (var i = 0; i < redovi.length; i++) {
            var red = { naziv: redovi[i].dataValues.naziv, postavka: redovi[i].dataValues.postavka };
        
            niz.push(red);
        }
        // uslov

        if (trebaJSON !== -1) {
            res.set('Content-Type', "application/json");
            res.send(JSON.stringify(niz));

        }
        else if (trebaXML !== -1 || trebaXML2 !== -1) {
        
            if (trebaXML !== -1) {
                res.set('Content-Type', 'application/xml');
            }
            else {
                res.set('Content-Type', 'text/xml');
            }
            var novi = '<?xml version="1.0" encoding="UTF-8"?>';
            novi += '<zadaci>';
            for (var i = 0; i < niz.length; i++) {
                novi += '<zadatak>';
                novi = novi + '<naziv>' + niz[i].naziv + '</naziv>';
                novi = novi + '<postavka>' + niz[i].postavka + '</postavka>';
                novi += '</zadatak>';
            }
            novi += '</zadaci>';
            res.send(novi);
        }
        else if (trebaCSV !== -1) {
            res.set('Content-Type', 'text/csv');
            var novi = '';
            for (var i = 0; i < niz.length; i++) {
                novi += niz[i].naziv + ', ';
                novi += niz[i].postavka + '\n';

            }

            res.send(novi);
        }
        else {

            res.set('Content-Type', "application/json");
            res.send(JSON.stringify(niz));
        }

    }).catch(function (err) {
        
        res.render('nemaZadatka', { naziv: 'Error', poruka: err });
    });


});


app.get('/postavke/:naziv', function (req, res) {
    if (req.params.naziv) {
        res.download('./postavke/' + req.params.naziv, req.params.naziv, function (err) {
            if (err) {
            } else {
            }
        });
    }
});

//__S4
//2.a i 2.b
app.post('/addVjezba', function (req, res) {
    
    if (req.body.sVjezbe) {
//2.a
        db.godina.findOne({ where: { id: req.body.sGodine } }).then(function (godina) {
            godina.getVjezbe().then(function (vjezbe) {
                
                for (var i = 0; i < vjezbe.length; i++) {
                 
                    if (vjezbe[i].id == req.body.sVjezbe) {
                        res.render('nemaZadatka', { naziv: 'Ima veza', poruka: 'veza već ima!' });
                    }
                }
                godina.addVjezbe(req.body.sVjezbe).then(() => {
                    res.sendFile(__dirname + '/stranice/addVjezba.html');
                }).catch(function (err) {
                    res.render('nemaZadatka', { naziv: 'Error', poruka: err });
                });
            }).catch(function (err) {
                res.render('nemaZadatka', { naziv: 'Ima veza', poruka: 'veza već ima!' });
            });
        }).catch(function (err) {

            res.render('nemaZadatka', { naziv: 'Ima veza', poruka: 'veza već ima!' });
        });

    }
    else {
        //2.b
        var vjezbaJeSpirala;

        if (req.body.spirala === undefined) {
            vjezbaJeSpirala = 0;
        }
        else {
            vjezbaJeSpirala = 1;
        }
        db.vjezba.findOne({ where: { naziv: req.body.naziv } }).then(function (vjezba) {

            if (vjezba == null) {

                db.vjezba.create({
                    naziv: req.body.naziv,
                    spirala: vjezbaJeSpirala
                })
                    .then(function (vjezba) {
                        vjezba.addGodine(req.body.sGodine);
                        vjezba.save();
                    });
                res.sendFile(__dirname + '/stranice/addVjezba.html');
            }
            else {
                res.render('nemaZadatka', { naziv: 'Ima vjezba', poruka: 'Vježba sa ovim nazivom već postoji!' });
            }
        });
    }
});

app.get('/dajIdZaGodinu', function (req, res) {
    var niz = [];

    db.godina.findAll({
        attributes: ['id', 'nazivGod']
    }).then(function (redovi) {
        for (var i = 0; i < redovi.length; i++) {
            var red = { id: redovi[i].dataValues.id, nazivGod: redovi[i].dataValues.nazivGod };
        
            niz.push(red);
        }


        res.writeHead(200, { 'Content-Type': "application/json" });
        res.end(JSON.stringify(niz));

    });
});
app.get('/dajIdZaVjezbu', function (req, res) {
    var niz = [];

    db.vjezba.findAll({
        attributes: ['id', 'naziv']
    }).then(function (redovi) {
        for (var i = 0; i < redovi.length; i++) {
            var red = { id: redovi[i].dataValues.id, naziv: redovi[i].dataValues.naziv };
     
            niz.push(red);
        }


        res.writeHead(200, { 'Content-Type': "application/json" });
        res.end(JSON.stringify(niz));

    });
});

app.post('/vjezba/:idVjezbe/zadatak', urlencodedParser, function (req, res) {

    if (req.body.sVjezbe && req.body.sZadatak && req.params.idVjezbe !== 0) {
        var idVjezbe1 = req.params.idVjezbe;

        db.vjezba.findOne({ where: { id: idVjezbe1 } }).then(function (pronadjenaVjezba) {
            if (pronadjenaVjezba) {
                db.zadatak.findOne({ where: { id: req.body.sZadatak } }).then(function (pronadjeniZadatak) {
                    if (pronadjeniZadatak) {
                        pronadjenaVjezba.addZadaci([pronadjeniZadatak]).then(function (dodaniRed) {
                            res.redirect('/addVjezba.html');
                        });
                    }
                    else {
                        res.render('greska', { url: '/addVjezba.html', poruka: 'Zadatak sa ovim id-om ne postoji!' });
                    }
                }).catch();
            }
            else {
                res.render('greska', { url: '/addVjezba.html', poruka: 'Vježba sa ovim id-om ne postoji!' });
            }
        }).catch();



    }
    else {
        res.render('greska', { url: '/addVjezba.html', poruka: 'Parametri nisu validni' });
    }
});


app.get('/dajZadatkeNele/:vjezbaID', function (req, res) {
    

    db.zadatak.findAll({
        attributes: ['id'],
        include: [{
            model: db.vjezba,
            as: 'vjezbe',
            where: { id: req.params.vjezbaID }
        }]
    }).then(function (rezultat) {
        var niz = [];
        for (var i = 0; i < rezultat.length; i++) {
            niz.push(rezultat[i].id);
        }
        if (!niz.length) {
            niz.push(-1);
        }
        db.zadatak.findAll({
            where: { id: { [Sequelize.Op.notIn]: niz } }
        }).then(function (rezultat) {
            res.send(rezultat);
        }).catch();

    });


});



app.post('/student', function (req, res) {
    var poslaniStudeni = req.body.studenti;
    var studentiFinal = [];

    for (var i = 0; i < poslaniStudeni.length; i++) {
        var postojiIstiIndex = studentiFinal.findIndex((student) => student.index == poslaniStudeni[i].index);
        if (postojiIstiIndex === -1) {
            studentiFinal.push(poslaniStudeni[i]);
        }
    }

    var brojDodanih = 0, brojUpisanih = 0;
    db.student.findAll().then(function (studentiIzBaze) {
        db.godina.findOne({ where: { id: req.body.godina } }).then(function (zeljenaGodina) {
            if (!zeljenaGodina) {
                res.render('nemaZadatka', { naziv: 'Godina ne postoji', poruka: 'godina sa proslijedjenim id-om ne postoji' });
            }
            else {
            
                for (var j = 0; j < studentiFinal.length; j++) {
                    var nadjeni = studentiIzBaze.find((student) => student.index == studentiFinal[j].index);
                    if (nadjeni) {
                        zeljenaGodina.addStudenti(nadjeni);
                        brojUpisanih += 1;
                    }
                    else {
                        brojUpisanih += 1;
                        brojDodanih = brojDodanih + 1;
                        db.student.create({ imePrezime: studentiFinal[j].imePrezime, index: studentiFinal[j].index }).then(function (dodaniStudent) {
                            zeljenaGodina.addStudenti(dodaniStudent);
                        }).catch(function (err) {
                            res.render('nemaZadatka', { naziv: 'error', poruka: err });
                        });


                    }
                }
                res.setHeader('Content-Type', 'application/json');
                var poruka = { message: 'Dodano je ' + brojDodanih + ' novih studenata i upisano ' + brojUpisanih + ' na godinu ' + zeljenaGodina.nazivGod };
                res.send(JSON.stringify(poruka));

            }
        }).catch(function (err) {
            res.render('nemaZadatka', { naziv: 'error', poruka: err });
        });
    }).catch(function (err) {
        res.render('nemaZadatka', { naziv: 'error', poruka: err });
    });


});
//Port
app.listen(8080, () => console.log('Spirala 4'));